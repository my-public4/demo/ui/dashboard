import React from "react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import {
	Table,
	TableRow,
	TableHead,
	TableBody,
	TableCell,
	Container,
	TableContainer,
	Button,
	Grid,
} from "@mui/material";
import { useQuery, gql } from "@apollo/client";

const GET_TRANSACTIONS = gql`
	query Transactions {
		transactions {
			amount
			bankName
			id
			type
		}
	}
`;

const GET_USER = gql`
	query User($userId: String) {
		user(id: $userId) {
			accountNumber
			balance
			displayName
			id
		}
	}
`;

const TABLE_HEAD = [
	{ id: "id", label: "Transaction ID", alignRight: false },
	{ id: "bankName", label: "Bank Name", alignRight: false },
	{ id: "amount", label: "Amount", alignRight: false },
	{ id: "type", label: "Type", alignRight: false },
];

const defaultTheme = createTheme();

export default function Dashboard() {
	const { loading, error, data } = useQuery(GET_TRANSACTIONS, {});

	const { data: userData } = useQuery(GET_USER, {
		variables: { userId: JSON.parse(localStorage.getItem("user"))?.id },
	});

	if (loading) return null;
	if (error) return `Error! ${error}`;

	const { transactions } = data;

	let user;

	if (userData) {
		user = userData.user;
	}

	return (
		<ThemeProvider theme={defaultTheme}>
			<Container component="main" maxWidth="xl">
				<Grid container spacing={2}>
					<Grid item xs={8}>
						<h1>Balance: {user?.balance}</h1>
					</Grid>
					<Grid item xs={4} textAlign={"right"}>
						<Button
							variant="outlined"
							onClick={() => {
								window.location = `/transactions/add`;
							}}
						>
							Add transaction
						</Button>
					</Grid>
				</Grid>

				<TableContainer sx={{ minWidth: 800 }}>
					<Table>
						<TableHead>
							<TableRow>
								{TABLE_HEAD.map((headCell) => (
									<TableCell
										key={headCell.id}
										align={headCell.alignRight ? "right" : "left"}
									>
										{headCell.label}
									</TableCell>
								))}
							</TableRow>
						</TableHead>
						<TableBody>
							{transactions.map((row) => {
								const { id, bankName, amount, type } = row;
								return (
									<TableRow hover key={id} tabIndex={-1} role="checkbox">
										<TableCell align="left">{id}</TableCell>

										<TableCell align="left">{bankName}</TableCell>

										<TableCell align="left">{amount}</TableCell>

										<TableCell align="left">{type}</TableCell>
									</TableRow>
								);
							})}
						</TableBody>
					</Table>
				</TableContainer>
			</Container>
		</ThemeProvider>
	);
}
